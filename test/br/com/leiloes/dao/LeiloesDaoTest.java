package br.com.leiloes.dao;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.leiloes.builders.LeilaoBuilder;
import br.com.leiloes.dominio.Leilao;
import br.com.leiloes.dominio.Usuario;

public class LeiloesDaoTest {
	
	private Session session;
	private UsuarioDao usuarioDao;
	private LeilaoDao leilaoDao;

	@Before
	public void init(){
		session = new CriadorDeSessao().getSession();
		leilaoDao = new LeilaoDao(session);
		usuarioDao = new UsuarioDao(session);
		
		session.beginTransaction();
	}
	
	@After
	public void finalize(){
		session.getTransaction().rollback();
		session.close();
	}
	
	@Test
    public void deveContarLeiloesNaoEncerrados() {
        // criamos um usuario
        Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");

        // criamos os dois leiloes
        Leilao ativo = new LeilaoBuilder().comNome("Geladeira").comValor(1500.0).comDono(mauricio).constroi();
        Leilao encerrado = new LeilaoBuilder().comNome("XBox").comValor(700.0).comDono(mauricio).encerrado().constroi();

        // persistimos todos no banco
        usuarioDao.salvar(mauricio);
        leilaoDao.salvar(ativo);
        leilaoDao.salvar(encerrado);

        // invocamos a acao que queremos testar
        // pedimos o total para o DAO
        long total = leilaoDao.total();

        assertThat(total, equalTo(1L));
    }
	
	@Test
	public void naoDeveContarLeiloesEncerrados() {
		Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
		
		Leilao encerrado1 = new LeilaoBuilder().comNome("Geladeira").comValor(1500.0).comDono(mauricio).encerrado().constroi();
        Leilao encerrado2 = new LeilaoBuilder().comNome("XBox").comValor(700.0).comDono(mauricio).encerrado().constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(encerrado1);
		leilaoDao.salvar(encerrado2);
		
		long total = leilaoDao.total();
		
		assertThat(total, equalTo(0L));
	}
	
	@Test
	public void deveContarApenasLeiloesNovos() {
		Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
		
		Leilao novo = new LeilaoBuilder().comNome("Geladeira").comValor(1500.0).comDono(mauricio).constroi();
		Leilao usado = new LeilaoBuilder().comNome("XBox").comValor(700.0).comDono(mauricio).usado().constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(novo);
		leilaoDao.salvar(usado);
		
		List<Leilao> leiloes = leilaoDao.novos();
		
		assertThat(leiloes.size(), equalTo(1));
		assertThat(leiloes.get(0).getNome(), equalTo(novo.getNome()));
	}
	
	@Test
	public void deveContarApenasLeiloesAntigos() {
		Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
		
		Leilao leilaoDeDezDiasAtras = new LeilaoBuilder().comNome("Geladeira Velha")
				.comValor(1500.0).comDono(mauricio).diasAtras(10).constroi();
		
		Leilao leilaoDeOitoDiasAtras = new LeilaoBuilder().comNome("Geladeira")
				.comValor(2500.0).comDono(mauricio).diasAtras(8).constroi();
		
		Leilao leilaoDeDoisDiasAtras = new LeilaoBuilder().comNome("XBox")
				.comValor(700.0).comDono(mauricio).diasAtras(2).constroi();
		
		Leilao leilaoDeHoje = new LeilaoBuilder().comNome("XBox X")
				.comValor(1000.0).comDono(mauricio).constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(leilaoDeDezDiasAtras);
		leilaoDao.salvar(leilaoDeOitoDiasAtras);
		leilaoDao.salvar(leilaoDeDoisDiasAtras);
		leilaoDao.salvar(leilaoDeHoje);
		
		List<Leilao> leiloes = leilaoDao.antigos();
		
		assertThat(leiloes.size(), equalTo(2));
		assertThat(leiloes.get(0).getNome(), equalTo(leilaoDeDezDiasAtras.getNome()));
		assertThat(leiloes.get(1).getNome(), equalTo(leilaoDeOitoDiasAtras.getNome()));
	}

	@Test
	public void deveContarLeiloesDeSeteDiasAtras() {
		Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
		
		Leilao leilaoDeSeteDiasAtras = new LeilaoBuilder().comNome("Geladeira")
				.comValor(2500.0).comDono(mauricio).diasAtras(7).constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(leilaoDeSeteDiasAtras);
		
		List<Leilao> leiloes = leilaoDao.antigos();
		
		assertThat(leiloes.size(), equalTo(1));
		assertThat(leiloes.get(0).getNome(), equalTo(leilaoDeSeteDiasAtras.getNome()));
	}
	
	@Test
    public void deveTrazerLeiloesNaoEncerradosNoPeriodo() {

        // criando as datas
        Calendar comecoDoIntervalo = Calendar.getInstance();
        comecoDoIntervalo.add(Calendar.DAY_OF_MONTH, -10);
        Calendar fimDoIntervalo = Calendar.getInstance();
        
        Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");

        // criando os leiloes, cada um com uma data
        Leilao leilao1 = new LeilaoBuilder().comNome("XBox")
				.comValor(700.0).comDono(mauricio).diasAtras(2).constroi();
        Leilao leilao2 = new LeilaoBuilder().comNome("Geladeira")
				.comValor(2500.0).comDono(mauricio).diasAtras(-20).constroi();

        // persistindo os objetos no banco
        usuarioDao.salvar(mauricio);
        leilaoDao.salvar(leilao1);
        leilaoDao.salvar(leilao2);

        // invocando o metodo para testar
        List<Leilao> leiloes = leilaoDao.porPeriodo(comecoDoIntervalo, fimDoIntervalo);

        // garantindo que a query funcionou
        assertThat(leiloes.size(), equalTo(1));
        assertThat(leiloes.get(0).getNome(), equalTo(leilao1.getNome()));
    }
	
	@Test
    public void naoDeveTrazerLeiloesEncerradosNoPeriodo() {

        // criando as datas
        Calendar comecoDoIntervalo = Calendar.getInstance();
        comecoDoIntervalo.add(Calendar.DAY_OF_MONTH, -10);
        Calendar fimDoIntervalo = Calendar.getInstance();

        Usuario mauricio = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");

        // criando os leiloes, cada um com uma data
        Leilao leilao1 = new LeilaoBuilder().comNome("XBox")
				.comValor(700.0).comDono(mauricio).diasAtras(2).encerrado().constroi();

        // persistindo os objetos no banco
        usuarioDao.salvar(mauricio);
        leilaoDao.salvar(leilao1);

        // invocando o metodo para testar
        List<Leilao> leiloes = leilaoDao.porPeriodo(comecoDoIntervalo, fimDoIntervalo);

        // garantindo que a query funcionou
        assertThat(leiloes.size(), equalTo(0));
    }
	
	@Test
    public void deveRetornarLeiloesDisputados() {
        Usuario mauricio = new Usuario("Mauricio", "mauricio@aniche.com.br");
        Usuario marcelo = new Usuario("Marcelo", "marcelo@aniche.com.br");

        Leilao leilao1 = new LeilaoBuilder()
                .comDono(marcelo)
                .comValor(3000.0)
                .comLance(Calendar.getInstance(), mauricio, 3000.0)
                .comLance(Calendar.getInstance(), marcelo, 3100.0)
                .constroi();

        Leilao leilao2 = new LeilaoBuilder()
                .comDono(mauricio)
                .comValor(3200.0)
                .comLance(Calendar.getInstance(), mauricio, 3000.0)
                .comLance(Calendar.getInstance(), marcelo, 3100.0)
                .comLance(Calendar.getInstance(), mauricio, 3200.0)
                .comLance(Calendar.getInstance(), marcelo, 3300.0)
                .comLance(Calendar.getInstance(), mauricio, 3400.0)
                .comLance(Calendar.getInstance(), marcelo, 3500.0)
                .constroi();

        usuarioDao.salvar(marcelo);
        usuarioDao.salvar(mauricio);
        leilaoDao.salvar(leilao1);
        leilaoDao.salvar(leilao2);

        List<Leilao> leiloes = leilaoDao.disputadosEntre(2500, 3500);

        assertThat(leiloes.size(), equalTo(1));
    }
	
	@Test
	public void deveListarLeiloesQueUmUsuarioDeuLanceSemRepeticoes() {
		Usuario mauricio = new Usuario("Mauricio", "mauricio@aniche.com.br");
		
		Leilao leilao1 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(1000.0)
				.comLance(Calendar.getInstance(), mauricio, 1000.0)
				.comLance(Calendar.getInstance(), mauricio, 2000.0)
				.constroi();
		
		Leilao leilao2 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(3200.0)
				.comLance(Calendar.getInstance(), mauricio, 3500.0)
				.constroi();
		
		Leilao leilao3 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(4000.0)
				.constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(leilao1);
		leilaoDao.salvar(leilao2);
		leilaoDao.salvar(leilao3);
		
		List<Leilao> leiloes = leilaoDao.listaLeiloesDoUsuario(mauricio);
		
		assertThat(leiloes.size(), equalTo(2));
	}
	
	@Test
	public void deveRecuperarValorInicialMedioDosLeiloesQueUmUsuarioDeuLance() {
		Usuario mauricio = new Usuario("Mauricio", "mauricio@aniche.com.br");
		
		Leilao leilao1 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(1000.0)
				.comLance(Calendar.getInstance(), mauricio, 1000.0)
				.comLance(Calendar.getInstance(), mauricio, 2000.0)
				.constroi();
		
		Leilao leilao2 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(3000.0)
				.comLance(Calendar.getInstance(), mauricio, 3500.0)
				.constroi();
		
		Leilao leilao3 = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(mauricio)
				.comValor(4000.0)
				.constroi();
		
		usuarioDao.salvar(mauricio);
		leilaoDao.salvar(leilao1);
		leilaoDao.salvar(leilao2);
		leilaoDao.salvar(leilao3);
		
		double valorInicialMedio = leilaoDao.getValorInicialMedioDoUsuario(mauricio);
		
		assertThat(valorInicialMedio, equalTo(2000.0));
	}
	
	@Test
    public void deveDeletarUmLeilao() {
        Usuario usuario = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
        
        Leilao leilao = new LeilaoBuilder()
				.comNome("Geladeira")
				.comDono(usuario)
				.comValor(1000.0)
				.constroi();

        usuarioDao.salvar(usuario);
        leilaoDao.salvar(leilao);
        leilaoDao.deleta(leilao);
        
        session.flush();

        assertNull(leilaoDao.porId(leilao.getId()));
    }
}
