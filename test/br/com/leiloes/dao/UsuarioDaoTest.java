package br.com.leiloes.dao;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.leiloes.dominio.Usuario;

public class UsuarioDaoTest {
	
	private Session session;
	private UsuarioDao usuarioDao;

	@Before
	public void init(){
		session = new CriadorDeSessao().getSession();
		usuarioDao = new UsuarioDao(session);
	}
	
	@After
	public void finalize(){
		session.close();
	}
	
	@Test
    public void deveEncontrarPeloNomeEEmailMockado() {
		Usuario usuarioInserido = new Usuario("João da Silva", "joao@dasilva.com.br");
		usuarioDao.salvar(usuarioInserido);
		
		Usuario usuarioRetornado = usuarioDao.porNomeEEmail(usuarioInserido.getNome(), usuarioInserido.getEmail());

        assertThat(usuarioRetornado.getNome(), equalTo(usuarioInserido.getNome()));
        assertThat(usuarioRetornado.getEmail(), equalTo(usuarioInserido.getEmail()));
    }
	
	@Test
	public void deveRetornarNullCasoUsuarioNaoExista() {
		Usuario usuarioNaoInserido = new Usuario("Outro João da Silva", "joao@dasilva.com.br");
		
		Usuario usuarioRetornado = usuarioDao.porNomeEEmail(usuarioNaoInserido.getNome(), usuarioNaoInserido.getEmail());
		
		assertNull(usuarioRetornado);
	}
	
	@Test
    public void deveDeletarUmUsuario() {
        Usuario usuario = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");

        usuarioDao.salvar(usuario);
        usuarioDao.deletar(usuario);
        
        session.flush();

        Usuario usuarioNoBanco = usuarioDao.porNomeEEmail("Mauricio Aniche", "mauricio@aniche.com.br");

        assertNull(usuarioNoBanco);
    }
	
	@Test
	public void deveAlterarUmUsuario() {
		Usuario usuario = new Usuario("Mauricio Aniche", "mauricio@aniche.com.br");
		usuarioDao.salvar(usuario);
		
		usuario.setNome("Outro Mauricio");
		usuarioDao.atualizar(usuario);
		
		session.flush();
		
		Usuario usuarioNoBanco1 = usuarioDao.porNomeEEmail("Outro Mauricio", "mauricio@aniche.com.br");
		Usuario usuarioNoBanco2 = usuarioDao.porNomeEEmail("Mauricio Aniche", "mauricio@aniche.com.br");
		
		assertThat(usuarioNoBanco1.getNome(), equalTo("Outro Mauricio"));
		assertNull(usuarioNoBanco2);
	}
}
